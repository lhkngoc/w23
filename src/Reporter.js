import React from "react";

function Reporter(props){
  return <div>{props.name}: {props.children}</div>;
};
export default Reporter;
