import logo from './logo.svg';
import './App.css';
import Reporter from './Reporter';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>

      <img src="https://images.uncyc.org/commons/1/12/Blaaa_1_.jpg"></img>
      <Reporter name="Antero Mertaranta">Löikö mörkö sisään</Reporter>
      <Reporter name="Abraham Lincoln">Whatever you are be a good one</Reporter>
    </div>
  );
}

export default App;
